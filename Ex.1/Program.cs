﻿using System;

namespace Ex._1
{
     class Program
     {
          static void Main(string[] args)
          {
               double c, x, y, z;
               x = 0.345;
               y = 0.570;
               c = 1.8;
               do
               {
                    double a = c * Math.Pow(Math.Sin(x - Math.PI / 3), 4);
                    double b = (1 / 7) + Math.Pow(Math.Log10(y), 2);

                    z = a / b;
                    Console.WriteLine(z);
                    x += 0.1;
               } while (x <= 7.45);

               Console.ReadLine();
          }
     }
}
